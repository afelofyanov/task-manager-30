package ru.tsc.felofyanov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.service.IUserService;
import ru.tsc.felofyanov.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    public IUserService getUserService() {
        return getServiceLocator().getUserService();
    }
}
