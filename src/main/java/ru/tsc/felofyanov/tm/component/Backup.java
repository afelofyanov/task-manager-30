package ru.tsc.felofyanov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.command.data.AbstractDataCommand;
import ru.tsc.felofyanov.tm.command.data.DataLoadBackupCommand;
import ru.tsc.felofyanov.tm.command.data.DataSaveBackupCommand;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 3000, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    public void save() {
        bootstrap.processCommand(DataSaveBackupCommand.NAME, false);
    }

    @SneakyThrows
    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.CATALOG)))
            Files.createDirectory(Paths.get(AbstractDataCommand.CATALOG));
        final boolean checkBinary = Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP));
        if (checkBinary) bootstrap.processCommand(DataLoadBackupCommand.NAME, false);
    }
}
