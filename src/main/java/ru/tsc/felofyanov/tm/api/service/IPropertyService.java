package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();
}
